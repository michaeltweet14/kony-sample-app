function AS_Houston_Init(eventobject) {
    return AS_Form_bcef9900bf1547a98dde2bef9b8b576a(eventobject);
}

function AS_Form_bcef9900bf1547a98dde2bef9b8b576a(eventobject) {
    helpers.log('init Called for ----' + this.id + '----');
    var views = {
        form: this,
        mainContent: this.MainContent
    };
    var controller = new mvc.OfficeController(views);
    mvc.register(this.id, this, controller);
}