function AS_Form1_init(eventobject) {
    return AS_Form_cfaacab787024ceea8b686831841a69c(eventobject);
}

function AS_Form_cfaacab787024ceea8b686831841a69c(eventobject) {
    helpers.log('init Called for ----' + this.id + '----');
    var views = {
        form: this,
        officeContainer: this.OfficeContainer
    };
    var controller = new mvc.Form1Controller(views);
    mvc.register(this.id, this, controller);
}