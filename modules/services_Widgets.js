//jshint esnext: true
widgets = {};
/********************************************************************
                          WIDGET BASE CLASS
********************************************************************/
widgets.Widget = function(){
  this.id = helpers.createGuid();
};

widgets.Widget.prototype.create = function(){
  //Main Flex Container
  var basicProperties = {
    id: "widget" + this.id,
    isVisible: true,
    width: "100%",
    zIndex: 1,
    clipBounds: true,
    skin: "white",
    autogrowMode: kony.flex.AUTOGROW_HEIGHT,
    layoutType: kony.flex.FLOW_VERTICAL,
    centerX: "50%",
    top: "5dp"
  };
  var layoutProperties = {};
  this.container = new kony.ui.FlexContainer(basicProperties, layoutProperties, {});
};
/********************************************************************
                      END WIDGET BASE CLASS
********************************************************************/
/********************************************************************
                             HEADER
********************************************************************/
widgets.Header = function(formController, headerName){
  var cloneTarget = '';
  widgets.Widget.call(this);
  if(formController.menuAdded) {
    cloneTarget = 'MenuHdr';
  }
  else {
    cloneTarget = 'BackHdr'; 
  }  
  var clone = TemplateHeader[cloneTarget].clone(this.id + '_');
  var views = {
    navButton: clone[this.id + '_' + cloneTarget + 'NavButton'],
    navImage: clone[this.id + '_' + cloneTarget + 'NavImage'],
    titleLabel: clone[this.id + '_' + cloneTarget + 'FormTitle']
  };
  var controller = new mvc.HeaderController(views, formController, headerName);
  mvc.register(formController.form.id, clone, controller);
  return clone;
};
//Set the Header to inherit from the Widget base class
widgets.Header.prototype = Object.create(widgets.Widget.prototype);
//Set the Header constructor to use that which is defined on the Header
widgets.Header.prototype.constructor = widgets.Header;
/********************************************************************
                            END HEADER
********************************************************************/
/********************************************************************
                              MENU
********************************************************************/
widgets.Menu = function(formController){
  widgets.Widget.call(this);
  this.cloneTarget = 'Menu';
  this.clone = TemplateMenu[this.cloneTarget].clone(this.id + '_');
  this.initController(formController);
  return this.clone;
};
//Set the Menu to inherit from the Widget base class
widgets.Menu.prototype = Object.create(widgets.Widget.prototype);
//Set the Menu constructor to use that which is defined on the Menu
widgets.Menu.prototype.constructor = widgets.Menu;
//Methods
widgets.Menu.prototype.initController = function(formController){
  var views = {
    buttonContainer: this.clone[this.id + '_ButtonContainer']
  };
  var controller = new mvc.MenuController(views, formController);
  mvc.register(formController.form.id, this.clone, controller);
};
/********************************************************************
                          END MENU
********************************************************************/
/********************************************************************
                         MENU BUTTON
********************************************************************/
widgets.MenuButton = function(){
  widgets.Widget.call(this);
  var cloneTarget = 'MenuButton';
  var clone = TemplateMenu[cloneTarget].clone(this.id + '_');
  return clone;  
};
//Set the MenuButton to inherit from the Widget base class
widgets.MenuButton.prototype = Object.create(widgets.Widget.prototype);
//Set the MenuButton constructor to use that which is defined on the MenuButton
widgets.MenuButton.prototype.constructor = widgets.MenuButton;
/********************************************************************
                        END MENU BUTTON
********************************************************************/
/********************************************************************
                        ERROR OVERLAY
********************************************************************/
widgets.ErrorOverlay = function(message) {
  widgets.Widget.call(this);
  var cloneTarget = 'ErrorMessage';
  var clone = TemplateError[cloneTarget].clone(this.id + '_');
  clone.text = message;
  return clone;
};
//Set the ErrorOverlay to inherit from the Widget base class
widgets.ErrorOverlay.prototype = Object.create(widgets.Widget.prototype);
//Set the ErrorOverlay constructor to use that which is defined on the ErrorOverlay
widgets.ErrorOverlay.prototype.constructor = widgets.ErrorOverlay;
/********************************************************************
                        END ERROR OVERLAY
********************************************************************/
/********************************************************************
                        Office Button
********************************************************************/
widgets.OfficeButton = function(office, image, onClickFunction) {
  widgets.Widget.call(this);
  helpers.debug(office + '     ' + image);
  var cloneTarget = 'OfficeContainer';
  var clone = TemplateOfficeButton[cloneTarget].clone(this.id + '_');
  clone[this.id + '_' + 'Skyline'].src = image;
  clone[this.id + '_' + 'Office'].text = office;
  clone.onClick = onClickFunction;
  return clone;
};
//Set the ErrorOverlay to inherit from the Widget base class
widgets.OfficeButton.prototype = Object.create(widgets.Widget.prototype);
//Set the ErrorOverlay constructor to use that which is defined on the ErrorOverlay
widgets.OfficeButton.prototype.constructor = widgets.OfficeButton;
/********************************************************************
                        END Office Button
********************************************************************/
/********************************************************************
                        Office Form
********************************************************************/
widgets.Office = function(office) {
  widgets.Widget.call(this);
  var cloneTarget = 'Office';
  helpers.debug('in Office Widget');
  var clone = TemplateOfficeForm[cloneTarget].clone(this.id + '_');
  helpers.debug('created clone :: ' + JSON.stringify(clone));
  clone[this.id + '_' + 'Description'].text = office.Description;
  helpers.debug('set description');
  clone[this.id + '_' + 'AddressText'].text = office.Address;
  helpers.debug('set Address');
  clone[this.id + '_' + 'PhoneText'].text = office.Phone;
  helpers.debug('set Phone');
  clone[this.id + '_' + 'FaxText'].text = office.Fax;
  helpers.debug('set Fax');
  clone[this.id + '_' + 'EmailText'].text = office.Email;
  helpers.debug('set Email');
  
  if (!office.Phone)
    clone[this.id + '_' + 'PhoneContainer'].isVisible = false;
  if (!office.Fax)
    clone[this.id + '_' + 'FaxContainer'].isVisible = false;
  helpers.debug('leaving widget');
  return clone;
};
//Set the ErrorOverlay to inherit from the Widget base class
widgets.Office.prototype = Object.create(widgets.Widget.prototype);
//Set the ErrorOverlay constructor to use that which is defined on the ErrorOverlay
widgets.Office.prototype.constructor = widgets.Office;
/********************************************************************
                        END Office Button
********************************************************************/