services = {};
CONSTANTS = {};
CONSTANTS.AppVersion = '1.0.0';
CONSTANTS.BaseForm = 'Form1'; //Initial form to load 
CONSTANTS.CacheTimeout = 180; //in minutes
CONSTANTS.CacheTimeoutFatal = 60*72; //in minutes
CONSTANTS.ApiTimeout = 120000; //in ms
CONSTANTS.DefaultForwardTransition = { transitionEffect: 'transitionMoveIn', transitionDirection: 'fromRight' };
CONSTANTS.DefaultBackTransition = { transitionEffect: 'transitionMoveOut', transitionDirection: 'fromRight' };
CONSTANTS.PortraitMode = 0;
CONSTANTS.LandscapeMode = 1;
CONSTANTS.MenuHierarchy = ['Houston', 'Dallas', 'Chicago', 'Los Angeles', 'New York', 'Philadelphia', 'Seattle', 'San Francisco', 'Washington DC', 'Atlanta'];
CONSTANTS.currentOffice = null;

CONSTANTS.imageMap = {
  'Houston' : 'houston_sky.png',
  'Dallas' : 'dallas_sky.png',
  'Chicago' : 'chicago_sky.png',
  'Los Angeles' : 'la_sky.png',
  'New York' : 'ny_sky.png',
  'Philadelphia' : 'phi_sky2.png',
  'Seattle' : 'sea_sky.png',
  'San Francisco' : 'sf_sky.png',
  'Washington DC' : 'dc_sky.png',
  'Atlanta' : 'atlanta_sky.png',
};
//reset user specific information
CONSTANTS.reset = function() {

};
 
/********************************************************************
Group: DAOs
Use: 
	-	Maps a string to the corresponding 
		Access call to retrieve that object
**********************************************************************/
CONSTANTS.DAOs = {
  'Offices' : {
    'Key': 'Offices',
    'Function' : 'webService.getOffices',
    'Parameters' : []
  }
};

/********************************************************************
Group: Navigation Hierarchy
Uses: 
	-	Defines the Form order for menu layout
    	-	Level 2 Forms show on the menu
    -	Defines the structure for navigation 
    	-	Navigating to a lower level form does not destroy the 
        	forms at a higher level
        -	Navigating to a higher level destroys all lower level forms
**********************************************************************/
CONSTANTS.navigationHierarchy = 
  [
  [{formId : 'Form1', displayText : ''}],
  [{formId : 'Office', displayText : 'Houston'}]
];

CONSTANTS.errors = {
  '504' : "We couldn't connect to the data server. Please try again later.",
  'UnavailableUsage' : 'Usage data is currently unavailable for this account.',
  'TouchId' : "We couldn't find a match for your finger! Please login with your username and password.",
  'MissingParameter' : 'Make sure all required fields are filled in.',
  'CustomerDoesNotExist' : 'Unable to find a user with that login.',
  'InvalidEmail' : 'Make sure you entered a valid email.',
  'Unauthorized' : 'Invalid User/App Credentials',
  'InvalidLogin' : 'Invalid username/password combination',
  'ExpiredCache' : 'We were unable to refresh your data. Please login and try again.',
  '103' : 'Invalid username/password combination',
  '500' : 'Unable to find a user with that login',
  '1000' : 'Error occurred when trying to connect to data server. Please try again later.',
  '1011' : "We couldn't find an active internet connection. Please try again once your connection is restored.",
  '1012' : 'Request failed. Please try again later.',
  '1013' : 'Invalid JSON was returned.',
  '1014' : 'Request timed out. Please try again later.',
  '1015' : 'Cannot find host. Please try again later.',
  '1016' : 'Cannot connect to host. Please try again later.',
  'Default' : 'Something went wrong, please try again later.'
};

/*********************
	Logging Strings
*********************/
CONSTANTS.baseHeader = '[SAMPLE APP]';
CONSTANTS.logHeader = CONSTANTS.baseHeader + '[INFO] ';
CONSTANTS.debugHeader = CONSTANTS.baseHeader + '[DEBUG] ';
CONSTANTS.warnHeader = CONSTANTS.baseHeader + '[WARN] ';
CONSTANTS.errHeader = CONSTANTS.baseHeader + '[ERROR] ';
/*********************
	Loading Strings
*********************/
CONSTANTS.refreshString = 'Refreshing Data...';
CONSTANTS.loginString = 'Logging In...';
CONSTANTS.submitString = 'Submitting...';
CONSTANTS.savingString = 'Saving...';
CONSTANTS.loadingString = 'Loading...';
/*********************
	Error Strings
    Note: These are stored in an Azure Storage Table and loaded on startup. 
    	  If they are not successfully retrieved the entries below are used
*********************/
CONSTANTS.errorHeader = 'Oops...';
