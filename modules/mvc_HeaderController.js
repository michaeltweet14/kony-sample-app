// jshint esnext: true
/********************************************************************
                         HEADER CONTROLLER CLASS
********************************************************************/
//Constructor
mvc.HeaderController = function(views, formController, headerName){
  mvc.ViewController.call(this, views, formController);
  //If there is no menu on the form we dont want to show a menu toggle.
  if (!this.formController.menuAdded) {
//    this.navImage.src = CONSTANTS.HeaderImages.BackBase;
	//Button Event Handlers    
    this.navButton.onTouchStart = (function(widget, x, y) {
      helpers.debug('onTouchStart');
      if (this.formController.orientation == CONSTANTS.PortraitMode) {
        x = parseInt(x.toString());
        y = parseInt(y.toString());
 //       this.navImage.src = CONSTANTS.HeaderImages.BackActive;
        helpers.startTouch(widget, x, y);
      }
    }).bind(this);
    this.navButton.onTouchEnd = (function(widget, x, y) {
      helpers.debug('onTouchEnd');
      if (this.formController.orientation == CONSTANTS.PortraitMode) {
        x = parseInt(x.toString());
        y = parseInt(y.toString());
//        this.navImage.src = CONSTANTS.HeaderImages.BackBase;
      }
      if(helpers.isTap(widget, x, y)) {
        helpers.debug('calling go back');
        this.formController.goBack();
      }
    }).bind(this);
  }
  else {
    //Button Event Handlers   
    this.navButton.onTouchStart = (function(widget, x, y) {
      if (this.formController.orientation == CONSTANTS.PortraitMode) {
        x = parseInt(x.toString());
        y = parseInt(y.toString());
//        this.navImage.src = CONSTANTS.HeaderImages.MenuActive;
        helpers.startTouch(widget, x, y);
      }
    }).bind(this);
    this.navButton.onTouchEnd = (function(widget, x, y) {
      if (this.formController.orientation == CONSTANTS.PortraitMode) {
        x = parseInt(x.toString());
        y = parseInt(y.toString());
   //     this.navImage.src = CONSTANTS.HeaderImages.MenuBase;
        if(helpers.isTap(widget, x, y)) this.formController.animateSlide(true);
      }
    }).bind(this);
  }
  //Create the Title (split the id on capital letters)
  this.titleLabel.text = headerName;
};
//Set the HeaderController to inherit from the ViewController base class
mvc.HeaderController.prototype = Object.create(mvc.ViewController.prototype);
//Set the HeaderController constructor to use that which is defined on the HeaderController
mvc.HeaderController.prototype.constructor = mvc.HeaderController;
/********************************************************************
                        END HEADER CONTROLLER CLASS
********************************************************************/
