
/********************************************************************
Group: App Config
Uses: 
 - Defines app level configuration keys that should only change
     between environments
**********************************************************************/
if (typeof(config) === "undefined") {
  config = {};
}

config.debugMode = true;

config.mobileFabric = {
    "IdentityService": "YOUR IDENTITY SERVICE NAME HERE",
    "IntegrationService": "YOUR INTEGRATION SERVICE NAME HERE"
};

config.KMSPROP = {
  "kmsServerUrl": "https://YOUR_ENVIRONMENT.messaging.konycloud.com", //kony messenger service name
  "senderID": "1235467890", // applicable for android only- clientID generated in google console
  "appId": "YOUR-APP-ID-HERE" //App Id from mobile fabric 
};
