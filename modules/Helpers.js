/**********************************************************************/
//						CUSTOM EXCEPTION CLASS
//							used for logging
/**********************************************************************/
function Exception(type, message) {
  this.type = type;
  this.message = message;
  this.stack = (new Error()).stack;
}

Exception.prototype = Object.create(Error.prototype);
Exception.prototype.name = "Exception";
Exception.prototype.constructor = Exception;

/**********************************************************************/
// jshint esnext: true
helpers = {
  touchLog : {}
};

/********************************************************************
  Function: officeNavigationHelper
  Params	: string
  Returns	: form
  Save the office we need data for and then return the "Office" form
  **********************************************************************/
helpers.officeNavigationHelper = function(officeName) {
  helpers.debug(officeName);
  CONSTANTS.currentOffice = officeName;
  return 'Office';
};

/********************************************************************
  Function: simpleAlert
  Params	: string message, const alertType
  Returns	:
  Displays a notification to the user.
  **********************************************************************/
helpers.simpleAlert = function(message, alertType, title){
  var config = {
    message: message,
    alertType: alertType,
    alertTitle: title
  };
  kony.ui.Alert(config, {});
};

/********************************************************************
  Function: createGuid
  Params	:
  Returns	: string Guid
  Returns a 16 character random identifier.
  **********************************************************************/
helpers.createGuid = function() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
    return v.toString(16);
  });
};

/********************************************************************
  Function: startTouch
  Params	: obj widget, int x, int y
  Returns	: boolean
  Stores the user click into the touchLog so we can determine if they
  were attempting to click a widget or starting to open the menu.
  **********************************************************************/
helpers.startTouch = function(widget, x, y) {
  var touchLength = 0.3;
  if(!(widget.id in helpers.touchLog)) {
    helpers.touchLog[widget.id] = { possibleTap: true, x: x, y: y };
    try
    {
      kony.timer.cancel(widget.id);
    }
    catch(e)
    {
      // suppress error
    }
    //If the user holds their click for more then [touchLength], this click is not a "tap".
    kony.timer.schedule(widget.id, function() {
      if(widget.id in helpers.touchLog && helpers.touchLog[widget.id].possibleTap !== undefined && helpers.touchLog[widget.id].possibleTap !== null) {
        helpers.touchLog[widget.id].possibleTap = false;
      }
    }, touchLength, false);
    //Otherwise its a tap.
    return true;
  }
  return false;
};

/********************************************************************
  Function: isTap
  Params	: obj widget, int x, int y
  Returns	: boolean
  Logic to determine if they user attempted to click a widget because
  not all widgets support an "onTap" event
  **********************************************************************/
helpers.isTap = function(widget, x, y) {
  var isTap = false;

  if(widget.id in helpers.touchLog &&
     //if the click has not been held down for a long time (determined by possible tap)
     helpers.touchLog[widget.id].possibleTap === true &&
     //and they do not move more than 5 pixels
     x >= helpers.touchLog[widget.id].x - 5 &&
     x <= helpers.touchLog[widget.id].x + 5 &&
     y >= helpers.touchLog[widget.id].y - 5 &&
     y <= helpers.touchLog[widget.id].y + 5) {
    //they performed a tap.
    isTap = true;
  }
  this.endTouch(widget, x, y);
  return isTap;
};

/********************************************************************
  Function: endTouch
  Params    : obj widget, int x, int y
  Returns    :
  Removes the widget clicks from the touchLog.
  **********************************************************************/
helpers.endTouch = function(widget, x, y) {
  if(widget.id in helpers.touchLog) {
    delete helpers.touchLog[widget.id];
  }
  try{
    kony.timer.cancel(widget.id);
  }
  catch(e){
    //suppress error
  }
};

/********************************************************************
  Function: executeDataRequest
  Params	: string, func[], obj[], func
  Returns	:
  Manages the loading screen display for all API calls.
  **********************************************************************/
helpers.executeDataRequest = function (spinnerText, asyncFunctionList, parametersList, successCallback, errorCallback, doShowLoadingScreen, showFatalError = true, doRefresh = false) {
  //helper function to dismiss loading screen
  var tryRemoveLoader = function(decrease) {
    if (decrease) {
      kony.application.dismissLoadingScreen();
    }
  };

  try {
    var context = this;
    var promises = [];
    helpers.debug('ExecutingDataRequest');
    if (doShowLoadingScreen) {
      kony.application.showLoadingScreen("LoadingContainer", spinnerText , constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, true, null);
    }

    if (asyncFunctionList.Length == parametersList.Length) {
      //Start each call in the function list
      asyncFunctionList.forEach(function(asyncFunction, index, arr) {
        promises.push(asyncFunction.apply(context, parametersList[index]));
      });
      //Wait for all calls to finish
      Q.allSettled(promises).timeout(CONSTANTS.ApiTimeout).then(function(values) {
        var errorPromises = values.filter(function (result) { return result.state != "fulfilled"; });
        if(errorPromises.length > 0) {
          var error = errorPromises[0].reason;
          helpers.debug('execute data request error: ' + JSON.stringify(error));
          //if our session token is expired
          if (error && error.code && error.code == 401 && doRefresh) {
            //we want to refresh the token
            helpers.debug('Token Expired... Refreshing the token');
            //reset the promise list
            promises = [];
            webService.refresh().timeout(CONSTANTS.ApiTimeout).then(function(response){
              helpers.debug('storing new authToken as :' + response);
              kony.store.setItem('authToken', response);
              //if we get a new token try the API calls again.
              asyncFunctionList.forEach(function(asyncFunction, index, arr) {
                promises.push(asyncFunction.apply(this, parametersList[index]));
              });
              //Wait for all calls to finish
              Q.allSettled(promises).timeout(CONSTANTS.ApiTimeout).then(function(values) {
                var errorPromises = values.filter(function (result) { return result.state != "fulfilled"; });
                tryRemoveLoader(doShowLoadingScreen);
                if(errorPromises.length > 0) {
                  helpers.debug('retry failed... oops.');
                  errorCallback(errorPromises[0].reason);
                } else {
                  helpers.debug('trying calls again');
                  if (successCallback) {
                    successCallback(values.map(function(x) { return x.value;}));
                  }
                }
              }, function(e){
                helpers.log("Exception in inner Q: " + JSON.stringify(e));
                error(error, false);
              });
            }).fail(function(error){
              helpers.debug("Failed to refresh token");
              helpers.debug('Refresh error: '+ JSON.stringify(error));
              helpers.fatalError(error, showFatalError);
            });
          }
          else {
            helpers.debug("Data request failed");
            helpers.debug('error: ' + JSON.stringify(error));
            tryRemoveLoader(doShowLoadingScreen);
            errorCallback(error);
          }

        } else {
          helpers.debug("Execute data function request complete successfully");
          tryRemoveLoader(doShowLoadingScreen);
          if (successCallback) {
            helpers.debug("Calling success callback");
            successCallback(values.map(function(x) { return x.value;}));
          }
        }
      }, function(e){helpers.log("Exception in outer Q: " + JSON.stringify(e)); helpers.error(e, true);});
    }
    else {
      tryRemoveLoader(doShowLoadingScreen);
      errorCallback('Function and Parameter count mismatch');
    }
  }
  catch(e){
    tryRemoveLoader(true);
    errorCallback(e);
  }
};


/********************************************************************
  Function: getErrorMessage
  Params	: object	error
  Returns	: string 	message
  Converts a system error into an easy to read string that can be shown
  to users.
  **********************************************************************/
helpers.getErrorMessage = function(error) {
  //Kony error codes can be in multiple places
  var code = error;
  if(error){
    code = error.type || error.errcode || error.code || error.opstatus || error.ErrorMessage;
  }
  var message = '';
  switch(code){
    case 'MissingParameter' :
      message = CONSTANTS.errors['MissingParameter'];
      break;
    case 'InvalidEmail' :
      message = CONSTANTS.errors['InvalidEmail'];
      break;
    case 'CustomerDoesNotExist' :
      message = CONSTANTS.errors['CustomerDoesNotExist'];
      break;
    case 'TouchIdFail' :
      message = CONSTANTS.errors['TouchId'];
      break;
    case 504 :
      message = CONSTANTS.errors['504'];
      break;
    case 103:
      message = CONSTANTS.errors['103'];
      break;
    case 1000:
      message = CONSTANTS.errors['1000'];
      break;
    case 1011:
      message = CONSTANTS.errors['1011'];
      break;
    case 1012:
      message = CONSTANTS.errors['1012'];
      break;
    case 1013:
      message = CONSTANTS.errors['1013'];
      break;
    case 1014:
      message = CONSTANTS.errors['1014'];
      break;
    case 1015:
      message = CONSTANTS.errors['1015'];
      break;
    case 1016:
      message = CONSTANTS.errors['1016'];
      break;
    default:
      //If we dont have a custom string, just show the default messsage.
      message = CONSTANTS.errors.Default;
  }
  return message;
};

/********************************************************************
  Function: getDevice
  Params	:
  Returns	: device object
  Creates an object with important device properties on it
  **********************************************************************/
helpers.getDevice = function () {
  var deviceInfo = kony.os.deviceInfo();
  var deviceId = helpers.getDeviceId();

  return {
    'deviceId' : deviceId,
    'deviceType' : deviceInfo.name,
    'deviceModel' : deviceInfo.model,
    'deviceVersion' : deviceInfo.version
  };
};

/********************************************************************
  Function: getDeviceId
  Params	:
  Returns	: string deviceId
  Identifies the unique device id assigned to the device in use.
  **********************************************************************/

helpers.getDeviceId = function () {
  var deviceId = '';
  var deviceInfo = kony.os.deviceInfo();
  var deviceName = deviceInfo.name;
  if (deviceName == 'android') {
    deviceId = deviceInfo.deviceid;
  }
  else if((deviceName=="iPhone")||(deviceName=="iPhone Simulator")||(deviceName=="iPad")||(deviceName=="iPad Simulator")) {
    var OSVersion = kony.os.toNumber(deviceInfo.osversion);
    if (OSVersion && OSVersion !== null && OSVersion < 6.0) {
      deviceId = deviceInfo.customdeviceid;
    }
    else {
      deviceId = deviceInfo.identifierForVendor;
    }
  }
  helpers.log('deviceId : ' + deviceId);
  return deviceId;
};

/********************************************************************
  Function: encryptString
  Params	:
  Returns	: string encryptedString
  Encrypts a string.
  **********************************************************************/
helpers.encryptString = function (string) {
  var encryptedString = '';
  encryptedString = kony.crypto.createHash('sha512', string);
  helpers.log('encryptedString : ' + encryptedString);
  return encryptedString;
};

/********************************************************************
  Function: showContainer
  Params	:
  Returns	:
  **********************************************************************/
helpers.showContainer = function (selectedContainer, containers) {
  if(containers){
    containers.forEach(function (container, index, array) {
      if (container == selectedContainer) {
        container.isVisible = true;
      }
      else {
        container.isVisible = false;
      }
    });
  }
  else{
    helpers.log("Tried to show a container when no containers exist!");
  }
};

/********************************************************************
Function: log
Params	: string	message 
Returns	: 
Logs an infromational string to the device.
**********************************************************************/
helpers.log = function(message) {
  kony.print(CONSTANTS.logHeader + message);
};

/********************************************************************
Function: debug
Params	: string	message 
Returns	: 
Logs an infromational string only when the app is in debug mode.
**********************************************************************/
helpers.debug = function(message) {
  if (config.debugMode) {
    kony.print(CONSTANTS.debugHeader + message);
  }
};

/********************************************************************
Function: warn
Params	: string	message 
Returns	: 
Logs a warning string to the device but only displays on the UI if
the device is in debug mode.
**********************************************************************/
helpers.warn = function (message) {
  kony.print(CONSTANTS.warnHeader + JSON.stringify(message));
  helpers.simpleAlert(helpers.getErrorMessage(message), constants.ALERT_TYPE_ERROR, CONSTANTS.errorHeader);
};

/********************************************************************
Function: error
Params	: obj	exception 
Returns	: 
Logs an error to the device and displays an easy to read message
to the user
**********************************************************************/
helpers.error = function(exception, displayExceptionMessage) {
  kony.print(CONSTANTS.errHeader + JSON.stringify(exception));
  var errorMessage;
  if(displayExceptionMessage){
    errorMessage = exception;
  }
  else {
    errorMessage = helpers.getErrorMessage(exception);
  }
  helpers.simpleAlert(errorMessage, constants.ALERT_TYPE_ERROR, CONSTANTS.errorHeader);
};

/********************************************************************
Function: fatalError
Params	: obj	exception 
Returns	: 
Logs an error to the device, displays an easy to read message
to the user, and redirects the user to the login page because
something catastrophic happened.
**********************************************************************/
helpers.fatalError = function(exception, showError = true) {
  var transition = { transitionEffect: 'transitionFade'};
  var form = eval(CONSTANTS.BaseForm);
  kony.print(CONSTANTS.errHeader + JSON.stringify(exception));
  form.inTransitionConfig =  transition;
  form.outTransitionConfig = transition;
  kony.application.dismissLoadingScreen();
  if(showError){
    helpers.simpleAlert(helpers.getErrorMessage(exception), constants.ALERT_TYPE_ERROR); 
  }
  helpers.clearLogoutCache();
  form.show();
};
