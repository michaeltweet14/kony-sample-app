//MOCK API Layer since we do not have a Mobile Fabric service for this POC
if (typeof(mock) === "undefined") {
  mock = {};
}

mock.API = function(){};

mock.API.prototype.getOffices = function() {
  return {
    code : 200,
    offices : [
      {
        Office : 'Houston',
        Address : '24 Greenway Plaza Suite 1717 Houston, TX 77046',
        Phone : '844.325.2729',
        Fax : '713.520.4290',
        Email : 'houston@parivedasolutions.com',
        Description : 'Our Houston office is one of our fastest growing offices and integral to the success of Pariveda Solutions by significantly expanding its market share. Passionately committed to our core values, our leadership team has deep ties to the community and businesses, leveraging their depth of knowledge and expertise to provide our clients with solutions that enable them to better manage their businesses. With significant industry experience in Oil and Gas, Energy, Manufacturing, Health Care and other industries, our team provides business critical solutions through Strategy, Process Design, Application Development and Business Intelligence projects.'
      },
      {
        Office : 'Dallas',
        Address : '2811 McKinney Ave Suite 220 Dallas, TX 75204',
        Phone : '844.325.2729',
        Fax : '214.855.1246',
        Email : 'dallas@parivedasolutions.com',
        Description : 'Dallas serves as the original and largest Pariveda office, consistently grown over the years by leveraging our strong relationships within our community and delivering work that meets and exceeds our clients’ expectations. From custom mobile applications and business intelligence portals to enterprise application architecture strategies and business systems plans, Dallas delivers a breadth and depth of knowledge and experience our clients continue to rely upon. Our commitment to client work carries over in our active community involvement with local charitable organizations.'
      },
      {
        Office : 'Atlanta',
        Address : '10 Tenth Street Suite 375 Atlanta, GA 30309',
        Phone : '844.325.2729',
        Fax : '404.528.1159',
        Email : 'atlanta@parivedasolutions.com',
        Description : 'Atlanta is one of North America’s fastest growing markets and finds itself easily connected to Europe, South America and Asia with access to more than 1,000 international businesses. Keeping our clients’ needs top of mind, the Atlanta office brings the markets’ top talent and a breadth of creative solutions from mobility, business intelligence, custom applications to strategy and business systems planning. The Atlanta team is focused on building lasting personal, community and business relationships that will sustain for all the years to come.'
      },
      {
        Office : 'New York',
        Address : '150 East 52nd Street Suite 8002 New York, NY 10022',
        Phone : '646.650.2171',
        Fax : '646.416.8200',
        Email : 'newyork@parivedasolutions.com',
        Description : 'The New York office is set in the Midtown East’s business center to provide services to clients in world financial centers. Our close proximity to hedge funds and other investment firms encourages close client collaboration in designing and developing advanced investment and trading software. New York is an important center for international affairs and is widely deemed the cultural capital of the world. It also exerts a significant impact upon global commerce, finance, media, art, fashion, research, technology, education, and entertainment. This high-energy city offers opportunities to develop a network of personal, community, and business relationships.'
      },
      {
        Office : 'Los Angeles',
        Address : '222 N Sepulveda Blvd Suite 2120 Los Angeles, CA 90245',
        Phone : '424.290.6000',
        Fax : '424.290.6001',
        Email : 'losangeles@parivedasolutions.com',
        Description : 'Los Angeles is the fifth largest IT services market in the world. The economy of Los Angeles is driven by international trade, entertainment (television, motion pictures, video games, recorded music), aerospace, technology, petroleum, fashion, apparel, and tourism. The contiguous ports of Los Angeles and Long Beach together comprise the fifth-busiest port in the world and the most significant port in the Western Hemisphere and is vital to trade within the Pacific Rim. With a wide array of industries we leverage our years of experience and dedication to excellence to build relationships not only with clients but also in the community.'
      },
      {
        Office : 'Chicago',
        Address : '20 N Wacker Dr. Suite 2620 Chicago, IL 60606',
        Phone : '844.325.2729',
        Fax : '312.902.5749',
        Email : 'chicago@parivedasolutions.com',
        Description : "Chicago is a rapidly growing office with a dynamic community of diverse neighborhoods, cultures, industries and businesses. The Chicago team excels in serving our clients, and enjoying the lifestyle options offered in the Chicagoland area providing everything from business intelligence solutions to IT strategies, to a broad range of customers in the Chicago metro area. As a major transportation hub, center of commerce, and home to many Fortune 500 companies' headquarters, Chicago offers opportunities to develop a network of personal, community, and business relationships that will allow a lifetime of service to our clients in Chicago and to the community at large."
      },
      {
        Office : 'Philadelphia',
        Address : '1601 Market Street 19th Floor Philadelphia, PA  19203',
        Phone : null,
        Fax : null,
        Email : 'philadelphia@parivedasolutions.com',
        Description : 'The Philadelphia office, like all ten of Pariveda’s locations, focuses on growing individuals (clients, their people, local communities) to their highest potentials by aligning business and technology to co-create solutions that drive change for their clients. The new office is located at 1601 Market Street in the heart of Philadelphia’s business district and just steps away from the Nation’s most historic area and world-class art museums.'
      },
      {
        Office : 'San Francisco',
        Address : '201 California St Suite 1250 San Francisco, CA 94111',
        Phone : '844.325.2729',
        Fax : '415.946.6101',
        Email : 'sf@parivedasolutions.com',
        Description : "The San Francisco office is located within the Financial District, which serves as its main central business district. The District is home to the city's largest concentration of corporate headquarters, law firms, banks and other financial institutions. The team in San Francisco has attracted a strong team of technology professionals that have built a culture around understanding the demands of the market conditions faced by our clients. Local industries include hedge funds, private equity, venture capital, investment management firms, real estate investment trusts, fund administrators and prime brokers – all of whom increasingly rely on software technology, often built specifically to meet unique analytic and accounting requirements, to stay ahead of changes in products, market conditions, global trading patterns, and investor communication needs. Our goal is to help grow the local team in a sustainable way."
      },
      {
        Office : 'Seattle',
        Address : '1100 Olive Way Suite 850 Seattle, WA 98101',
        Phone : '844.325.2729',
        Fax : '206.673.2090',
        Email : 'seattle@parivedasolutions.com',
        Description : 'Seattle consistently ranks as one of the most highly educated cities in the nation and home to some of the most recognizable global technology companies. Seattle is thriving with established industry sectors including aerospace, information technology and retail as well as emerging industry segments, such as interactive media, music and clean technology. We partner with our clients to solve big problems in integration and data management using development expertise in security and privacy user experience (also known as Trust UX), change management, wireless systems testing and score card development.  The same dedication and commitment we provide our clients can be seen through our involvement in the local community with organizations like Roots Young Adult Shelter and Operation Sack Lunch.'
      },
      {
        Office : 'Washington DC',
        Address : '1616 North Fort Myer Dr. Suite 1110 Washington DC, VA 22209',
        Phone : '844.325.2729',
        Fax : '571.321.5968',
        Email : 'dc@parivedasolutions.com',
        Description : "Located in Northern Virginia, just outside our Nation’s Capital, the DC office encompasses the District of Columbia, Southern Maryland, Northern Virginia, and Richmond. DC is home to numerous Fortune 500 companies, not to mention the Federal Government.  With a diverse economy, the region is also host to a variety of industries including manufacturing, financial services, pharmaceutical, telecommunications, hospitality, consumer products, and technology. DC's  solutions to our clients in the areas of Business Systems Planning, Project Management, Project Recovery, IT Strategy, Software Selection and Negotiation, Systems Implementation and many others cultivates lasting relationships as we grow our presence with clients and also in the community."
      }
    ]
  }; 
};

var mockApi = new mock.API();