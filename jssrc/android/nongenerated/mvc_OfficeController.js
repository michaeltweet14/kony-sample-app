// jshint esnext: true
/********************************************************************
                         Office CONTROLLER CLASS
********************************************************************/
//Constructor
mvc.OfficeController = function(views) {
    mvc.FormController.call(this, views, false, CONSTANTS.currentOffice);
    this.getData();
};
//Set the Controller to inherit from the FormController base class
mvc.OfficeController.prototype = Object.create(mvc.FormController.prototype);
//Set the Controller constructor to use that which is defined on the OfficeController
mvc.OfficeController.prototype.constructor = mvc.OfficeController;
/****************
    METHODS
****************/
mvc.OfficeController.prototype.getData = function() {
    var context = this;
    var showSpinner = true;
    var dataFunctions = [CONSTANTS.DAOs.Offices];
    DAL.getValidData(dataFunctions, showSpinner).then(function(values) {
        context.populateForm(values[0]);
    }).fail(function(error) {
        context.populateErrors();
        helpers.error(error);
    });
};
mvc.OfficeController.prototype.populateForm = function(offices) {
    var context = this;
    var widget;
    helpers.debug('in populate form :: ' + JSON.stringify(offices));
    helpers.debug(CONSTANTS.currentOffice);
    var officeData = offices.filter(function(office) {
        return office.Office == CONSTANTS.currentOffice;
    })[0];
    helpers.debug('office data :: ' + JSON.stringify(officeData));
    var dataWidget = new widgets.Office(officeData);
    context.mainContent.addAt(dataWidget, 0);
};
mvc.OfficeController.prototype.populateErrors = function() {
    var context = this;
    helpers.debug('in populateErrors');
};
/********************************************************************
                        END HOME CONTROLLER CLASS
********************************************************************/