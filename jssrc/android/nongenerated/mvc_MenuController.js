// jshint esnext: true
/********************************************************************
                        MENU CONTROLLER CLASS
********************************************************************/
mvc.MenuController = function(views, formController) {
    mvc.ViewController.call(this, views, formController);
    var context = this;
    var MENU_TEXT_ACTIVE_SKIN = 'MenuButtonTextActive';
    var MENU_TEXT_BASE_SKIN = 'MenuButtonTextBase';
    CONSTANTS.MenuHierarchy.forEach(function(form) {
        try {
            helpers.log('CREATING BUTTON');
            var button = new widgets.MenuButton();
            helpers.log('BUTTON MADE');
            var guid = button.id.split('_')[0];
            button[guid + '_ButtonText'].text = form;
            button.onTouchStart = (function(widget, x, y) {
                button[guid + '_ButtonText'].skin = MENU_TEXT_ACTIVE_SKIN;
                x = parseInt(x.toString());
                y = parseInt(y.toString());
                helpers.startTouch(widget, x, y);
            }).bind(this);
            button.onTouchEnd = (function(widget, x, y) {
                button[guid + '_ButtonText'].skin = MENU_TEXT_BASE_SKIN;
                x = parseInt(x.toString());
                y = parseInt(y.toString());
                if (helpers.isTap(widget, x, y)) {
                    try {
                        context.formController.navigateTo(eval(helpers.officeNavigationHelper(form)));
                    } catch (e) {
                        helpers.error('Failed to navigate to form "' + form + '": ' + JSON.stringify(e));
                    }
                }
            }).bind(this);
            context.buttonContainer.add(button);
        } catch (e) {
            helpers.debug(JSON.stringify(e));
            helpers.error('Failed to inject menu button for form "' + form + '".');
        }
    }, this);
};
//Set the MenuController to inherit from the ViewController base class
mvc.MenuController.prototype = Object.create(mvc.ViewController.prototype);
//Set the MenuController constructor to use that which is defined on the MenuController
mvc.MenuController.prototype.constructor = mvc.MenuController;
/********************************************************************
                       END MENU CONTROLLER CLASS
********************************************************************/