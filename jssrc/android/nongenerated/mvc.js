/******************************************************************************************************'
										mvc.js
Purpose: Defines MVC framework used throughout the app
*******************************************************************************************************/
// jshint esnext: true
mvc = {
    lastPage: null,
    formDestroyQueue: []
};
mvc.controllers = {};
/********************************************************************
Function: register
Params	: string, obj, obj
Returns	: 
Enables tracking of view-controller associations
**********************************************************************/
mvc.register = function(formId, widget, controller) {
    try {
        var key = formId + '.' + widget.id;
        this.controllers[key] = controller;
    } catch (e) {
        helpers.error(e);
    }
};
/********************************************************************
Function: unregister
Params	: string, obj
Returns	: 
Disables tracking of view-controller associations
**********************************************************************/
mvc.unregister = function(formId, widget) {
    try {
        var key = formId + '.' + widget.id;
        try {
            widget.widgets().forEach(function(childWidget) {
                this.unregister(formId, childWidget);
            }, this);
        } catch (e) {
            // suppress error
            helpers.log("ERROR IN UNREGISTER: " + JSON.stringify(e));
        }
        if (this.controllers[key]) {
            delete this.controllers[key];
        }
    } catch (e) {
        helpers.error(e);
    }
};
/********************************************************************
                          CONTROLLER CLASS
********************************************************************/
//Constructor
mvc.Controller = function(views) {
    for (var viewName in views) {
        this[viewName] = views[viewName];
    }
};
/********************************************************************
                        END CONTROLLER CLASS
********************************************************************/
/********************************************************************
                       FORM CONTROLLER CLASS
Base form controller class - has specific properties needed for each form
********************************************************************/
//Constructor
mvc.FormController = function(views, addMenu, headerName = null) {
    helpers.checkArgs(views, ['form']);
    //Calls base constructor
    mvc.Controller.call(this, views);
    if (addMenu) {
        this.addMenu();
        this.menuAdded = true;
        this.slideArea = this.form.MainContent.parent;
        this.form.preShow = (function() {
            this.slideArea.left = 0;
        }).bind(this);
    } else {
        this.menuAdded = false;
    }
    if (headerName) {
        this.addHeader(headerName);
    }
    this.orientation = CONSTANTS.PortraitMode; //Forms default to portrait mode 
    this.form.postShow = (function() {
        this.postShow(addMenu);
    }).bind(this); //Injects the menu after form is loaded
    this.form.onHide = (function() {
        mvc.lastPage = this;
    }).bind(this); //Sets global so back button navigates here
};
//Set the FormController to inherit from the Controller base class
mvc.FormController.prototype = Object.create(mvc.Controller.prototype);
//Set the FormController constructor to use that which is defined on the FormController
mvc.FormController.prototype.constructor = mvc.FormController;
/****************
    METHODS
****************/
mvc.FormController.prototype.initForm = function() {};
/********************************************************************
Function: addMenu
Params	: 
Returns	: 
Creates a menu and and adds it to the Form related to the FormController
**********************************************************************/
mvc.FormController.prototype.addMenu = function() {
    //Constant for how wide the menu slider should be 
    var SLIDE_WIDTH = 0.1; //10%
    //Creates the UI object
    var menu = new widgets.Menu(this);
    //Adds it to the form
    this.form.addAt(menu, 0);
    //Configs for slide animation
    this.slideState = {
        screenWidth: kony.os.deviceInfo().screenWidth,
        sliding: false,
        animating: false
    };
    this.slideState.leftBound = this.slideState.screenWidth * SLIDE_WIDTH;
    this.slideState.rightBound = this.slideState.screenWidth - this.slideState.leftBound;
    this.form.onTouchEnd = this.endSlide.bind(this);
    //Slide event handlers
    this.form.MainContent.onTouchStart = this.startSlide.bind(this);
    this.form.MainContent.onTouchEnd = function(widget, x, y) {
        x = parseInt(x.toString());
        y = parseInt(y.toString());
        helpers.endTouch(widget, x, y);
    };
    this.form.onTouchStart = function(widget, x, y) {
        x = parseInt(x.toString());
        y = parseInt(y.toString());
        helpers.startTouch(widget, x, y);
    };
    this.form.onTouchMove = this.slide.bind(this);
};
/********************************************************************
Function: addHeader
Params	: 
Returns	: 
Creates a header widget and adds it to the Form.
**********************************************************************/
mvc.FormController.prototype.addHeader = function(headerName) {
    //Create the UI object
    var header = new widgets.Header(this, headerName);
    //Makes room for the Header
    this.form.MainContent.height = '93%';
    //Adds it to the form
    this.form.ContentArea.addAt(header, 0);
};
/********************************************************************
Function: postShow
Params	: 
Returns	: 
Handles destruction logic of the Form and its children.
**********************************************************************/
mvc.FormController.prototype.postShow = function(addMenu) {
    var context = this;
    if (mvc.lastPage && !('cameFrom' in context)) {
        context.cameFrom = mvc.lastPage.form;
        helpers.debug('setting cameFrom obj to  :: ' + JSON.stringify(mvc.lastPage.form.id));
    }
    context.goingBack = false;
    var destroyForm = function(heirarchyObject) {
        helpers.debug(JSON.stringify(heirarchyObject));
        var formId = heirarchyObject.formId;
        if (formId != this.form.id) {
            helpers.debug("About to Destroy Form " + formId);
            if ((formId + '.' + formId) in mvc.controllers || forceDestroy) {
                helpers.debug("Destroying Form " + formId);
                var form = eval(formId);
                mvc.unregister(formId, form);
                form.destroy();
                helpers.debug("Destroyed Form " + formId);
            }
        } else {
            helpers.debug("Not Destroying Form " + formId);
            foundThisForm = true;
        }
    };
    var foundThisForm = false,
        forceDestroy = true;
    mvc.formDestroyQueue.forEach(destroyForm, context);
    mvc.formDestroyQueue = [];
    // reset form destroy loop vars
    foundThisForm = false;
    forceDestroy = false;
    var navigation = CONSTANTS.navigationHierarchy;
    var formNames, level;
    for (level = navigation.length - 1; foundThisForm === false; --level) {
        if (level < 0) {
            helpers.debug('Form "' + context.form.id + '" is not in the navigation hierarchy.  It will be destroyed when navigating away.');
            mvc.formDestroyQueue.push(context.form.id);
            break;
        }
        formNames = navigation[level];
        helpers.debug('Starting to destroy forms: ' + JSON.stringify(formNames));
        formNames.forEach(destroyForm, context);
    }
    helpers.debug('finished destroying forms');
};
/********************************************************************
Function: navigateTo
Params	: obj
Returns	: 
Handles animation logic for navigating to Forms
**********************************************************************/
mvc.FormController.prototype.navigateTo = function(form, inTransition, outTransition) {
    var transition = CONSTANTS.DefaultForwardTransition;
    if (this.goingBack) transition = CONSTANTS.DefaultBackTransition;
    this.form.inTransitionConfig = transition;
    this.form.outTransitionConfig = transition;
    form.inTransitionConfig = inTransition || transition;
    form.outTransitionConfig = outTransition || transition;
    form.show();
};
/********************************************************************
Function: goBack
Params	: 
Returns	: 
Handles navigation logic for subForms (forms that are lower in the nav
hierarchy then the previous visited form.)
**********************************************************************/
mvc.FormController.prototype.goBack = function() {
    helpers.debug('in GOBACK');
    try {
        this.goingBack = true;
        helpers.debug('calling navigate to ::: ' + JSON.stringify(this.cameFrom));
        this.navigateTo(this.cameFrom);
    } catch (e) {
        helpers.error(e);
    }
};
/********************************************************************
Function: animateSlide
Params	: boolean
Returns	: 
Animates the menu slide open/close
**********************************************************************/
mvc.FormController.prototype.animateSlide = function(toggleMenu) {
    var OPEN_BOUND = 0.5; //How far the slider needs to come to open the menu fully
    //You can't click the menu flyout while sliding the menu.
    if (this.slideState.sliding === false) {
        this.slideState.animating = true;
        var left = toggleMenu ? this.slideState.rightBound : 0;
        //if the current position is >50% of the screen, we want to open the menu.
        if (this.slideArea.left > this.slideState.screenWidth * OPEN_BOUND) {
            left = toggleMenu ? 0 : this.slideState.rightBound;
        }
        //Animation to finish opening/closing menu
        var animationObj = kony.ui.createAnimation({
            100: {
                left: left,
                stepConfig: {
                    timingFunction: kony.anim.EASE
                }
            }
        });
        var animationConfig = {
            delay: 0,
            iterationCount: 1,
            fillMode: kony.anim.FILL_MODE_FORWARDS,
            duration: 0.2
        };
        this.slideArea.animate(animationObj, animationConfig, {
            animationEnd: (function() {
                this.slideState.animating = false;
            }).bind(this)
        });
    }
};
/********************************************************************
Function: startSlide
Params	: object, obj/int, obj/int
Returns	: 
onTouchStart event for menu slide
**********************************************************************/
mvc.FormController.prototype.startSlide = function(widget, x, y) {
    if (this.orientation == CONSTANTS.PortraitMode) {
        //Android sends the coords and objects, iOS sends them as ints
        x = parseInt(x.toString());
        y = parseInt(y.toString());
        if (helpers.startTouch(widget, x, y) && x < this.slideState.leftBound && this.slideState.sliding === false && this.slideState.animating === false) {
            this.slideState.sliding = true;
            try {
                if (this.form.MainContent.enableScrolling === true) {
                    this.form.MainContent.enableScrolling = false;
                    helpers.debug('Scrolling disabled on "' + this.form.id + '".');
                }
            } catch (e) {
                // suppress error
            }
        }
    }
};
/********************************************************************
Function: slide
Params	: object, obj/int, obj/int
Returns	: 
Moves the Container with the users click location
**********************************************************************/
mvc.FormController.prototype.slide = function(widget, x, y) {
    x = parseInt(x.toString());
    y = parseInt(y.toString());
    if (this.slideState.sliding === true) {
        this.slideArea.left = x;
        this.form.forceLayout();
    }
};
/********************************************************************
Function: endSlide
Params	: object, obj/int, obj/int
Returns	: 
onTouchEnd event for menu slide
**********************************************************************/
mvc.FormController.prototype.endSlide = function(widget, x, y) {
    helpers.log('in Endslide');
    x = parseInt(x.toString());
    y = parseInt(y.toString());
    helpers.endTouch(widget, x, y);
    if (this.slideState.sliding) {
        this.slideState.sliding = false;
        try {
            if (this.form.MainContent.enableScrolling === false) {
                this.form.MainContent.enableScrolling = true;
                helpers.debug('Scrolling enabled on "' + this.form.id + '".');
            }
        } catch (e) {
            // suppress error
        }
        this.animateSlide(false);
    }
};
/********************************************************************
                     END FORM CONTROLLER CLASS
********************************************************************/
/********************************************************************
                       VIEW CONTROLLER CLASS
********************************************************************/
//Constructor
mvc.ViewController = function(views, formController) {
    mvc.Controller.call(this, views);
    this.formController = formController;
};
//Set the ViewController to inherit from the Controller base class
mvc.ViewController.prototype = Object.create(mvc.Controller.prototype);
//Set the ViewController constructor to use that which is defined on the ViewController
mvc.ViewController.prototype.constructor = mvc.ViewController;
/********************************************************************
                      END VIEW CONTROLLER CLASS
********************************************************************/