//jshint esnext:true
/********************************************************************
                             WEB SERVICE
********************************************************************/
services.WebService = function() {};
/****************
    METHODS
****************/
/**
 * Example API call using Mobile Fabric
 *
 */
// services.WebService.prototype.getMinimumVersion = function() {
//   var deferred = Q.defer();
//   try {
//     var params = {};
//     var mobileService = KNYMobileFabric.getIntegrationService(config.mobileFabric.MobileService);
//     helpers.debug("Getting minimum version");
//     mobileService.invokeOperation("GetMinimumVersion", {}, params, 
//                                   function(response){ 
//       if (response && response.code == 200) {
//         helpers.debug("Minimum Version: " + JSON.stringify(response));          
//         deferred.resolve(response.MinimumVersion); 
//       }
//       else {
//         deferred.reject(response);
//       }
//     }, 
//                                   function(error){ 
//       deferred.reject(error);
//     });
//   }
//   catch(e){
//     deferred.reject(e);
//   }
//   return deferred.promise;
// };
services.WebService.prototype.getOffices = function() {
    var deferred = Q.defer();
    try {
        helpers.debug("Calling API to retrieve offices");
        var response = mockApi.getOffices();
        if (response && response.code == 200) {
            helpers.debug('Offices :: ' + JSON.stringify(response));
            deferred.resolve(response.offices);
        } else {
            helpers.debug('GetOffices ERROR: ' + JSON.stringify(response));
            deferred.resolve(response);
        }
    } catch (e) {
        helpers.debug("GetOffices Something bad happened!" + JSON.stringify(e));
        deferred.reject(e);
    }
    return deferred.promise;
};
var webService = new services.WebService();
/********************************************************************
                           END  WEB SERVICE
********************************************************************/