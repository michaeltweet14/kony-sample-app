//jshint esnext:true
/********************************************************************
                           DATA ACCESS LAYER
  ********************************************************************/
services.DAL = function() {};
/****************
      METHODS
  ****************/
/********************************************************************
  Function: getValidData
  Params	: DAO	bool 
  Returns	: data
  Checks the local cache for data. If data exists and is not expired return
  the data from the cache. Otherwise call the webservice to retrieve the data.
  **********************************************************************/
services.DAL.prototype.getValidData = function(DAOList, showSpinner, doRefresh = true, minuteTimeout = CONSTANTS.CacheTimeout) {
    var deferred = Q.defer();
    var context = this;
    var resultList = [];
    var notInCacheIndexQueue = [];
    var indexQueuePointer = 0;
    if (DAOList && DAOList.length > 0) {
        var dataCallsToMake = [];
        //First check the cache for each item in the list
        //if it exists add it to the results
        //else push the DAO onto another array to fetch the data
        var cachedObjects = [];
        DAOList.forEach(function(DAO, index) {
            helpers.log('retrieving ' + DAO.Key);
            var cacheObject = JSON.parse(kony.store.getItem(DAO.Key));
            cachedObjects.push(cacheObject);
            helpers.debug(DAO.Key + ' CACHED TIME: ' + (cacheObject ? new Date(cacheObject.timestamp) : 'N/A'));
            helpers.debug(DAO.Key + ' TIME TO COMPARE: ' + new Date(Date.today().setTimeToNow().addMinutes((-1) * minuteTimeout)));
            //if the time the object was cached was before (Now - configurable set of time) the object is still valid, otherwise we need to fetch new data  
            if (cacheObject && new Date(cacheObject.timestamp) > Date.today().setTimeToNow().addMinutes((-1) * minuteTimeout)) {
                helpers.debug('cached object found');
                resultList.push(cacheObject.data);
                //     helpers.debug('object resolved: ' + JSON.stringify(cacheObject.data));
            } else {
                dataCallsToMake.push(DAO);
                notInCacheIndexQueue.push(resultList.length);
                resultList.push(null);
            }
        });
        //if there are any data calls to make
        if (dataCallsToMake.length >= 1) {
            try {
                helpers.log("Couldnt find data in cache: " + JSON.stringify(dataCallsToMake));
                var functions = [];
                var params = [];
                dataCallsToMake.forEach(function(DAO) {
                    if (DAO.Function && DAO.Parameters) {
                        functions.push(eval(DAO.Function));
                        params.push(DAO.Parameters);
                    } else {
                        helpers.log("Couldn't find DAO Function or Parameters for " + DAO.Key);
                        throw new Exception("Couldn't find DAO Function or Parameters for " + DAO.Key);
                    }
                });
                helpers.debug('EXECUTING DATA REQUEST');
                helpers.executeDataRequest(CONSTANTS.loadingString, functions, params, function(results) {
                    helpers.log("Data requests successful, storing data in cache");
                    dataCallsToMake.forEach(function(DAO, index) {
                        context.setData(DAO.Key, results[index]);
                        var resultListIndex = notInCacheIndexQueue[indexQueuePointer];
                        indexQueuePointer++;
                        resultList[resultListIndex] = results[index];
                        //resultList.push(results[index]);
                    });
                    deferred.resolve(resultList);
                }, function(error) {
                    deferred.reject(error);
                }, (showSpinner === null || typeof(showSpinner) == "undefined") ? true : showSpinner, true, doRefresh);
            } catch (e) {
                helpers.log("Something went wrong making data calls: " + JSON.stringify(e));
                helpers.debug('requesting data exception: ' + JSON.stringify(e));
                //return the expired data, but don't update the timeout.
                var objectsToReturn = [];
                cachedObjects.forEach(function(cachedObject) {
                    if (cachedObject && cachedObject.timestamp > Date.today().setTimeToNow().addMinutes(-1 * CONSTANTS.CacheTimeoutFatal)) {
                        objectsToReturn.push(cachedObject.data);
                    } else {
                        helpers.error({
                            'code': 'ExpiredCache',
                            'message': 'Cached object older than configured timeout.'
                        });
                    }
                });
                deferred.resolve(objectsToReturn);
            }
        }
        //else there are no data calls to make
        //resolve the results
        else {
            deferred.resolve(resultList);
        }
    } else {
        deferred.reject("No DAOs provided!");
    }
    return deferred.promise;
};
/********************************************************************
  Function: getFromCache
  Params	: key
  Returns	: data
  Checks the local cache for data. If data exists we return it, otherwise return null
  **********************************************************************/
services.DAL.prototype.getFromCache = function(key) {
    var deferred = Q.defer();
    try {
        var obj = JSON.parse(kony.store.getItem(key));
        if (obj && obj.data) {
            deferred.resolve(obj.data);
        } else {
            deferred.reject("Object not in cache: " + key);
        }
    } catch (e) {
        deferred.reject(e);
    }
    return deferred.promise;
};
/********************************************************************
  Function: expireCache
  Params	: key	 
  Returns	: 
  Saves data to the cache with the current timestamp
  **********************************************************************/
services.DAL.prototype.expireCache = function(key) {
    var cacheObject = JSON.parse(kony.store.getItem(key));
    if (cacheObject) {
        cacheObject.timestamp = null;
        kony.store.setItem(key, JSON.stringify(cacheObject));
    }
};
/********************************************************************
  Function: setData
  Params	: key	data 
  Returns	: 
  Saves data to the cache with the current timestamp
  **********************************************************************/
services.DAL.prototype.setData = function(key, obj) {
    var deferred = Q.defer();
    try {
        var storageObject = {
            "data": obj,
            "timestamp": Date.today().setTimeToNow()
        };
        helpers.debug('STORING ' + key + ' AS ' + JSON.stringify(storageObject));
        kony.store.setItem(key, JSON.stringify(storageObject));
        //     helpers.debug('STORED ' + key + ' AS ' + JSON.stringify(storageObject));
        deferred.resolve();
    } catch (e) {
        helpers.error(e);
        deferred.reject(e);
    }
    return deferred.promise;
};
/********************************************************************
  Function: clearWebCache
  Params	:  
  Returns	: 
  Clears the cache of all web data (username will not be cleared.)
  **********************************************************************/
services.DAL.prototype.clearWebCache = function() {
    var itemsToRemove = Object.keys(CONSTANTS.DAOs);
    helpers.debug(itemsToRemove);
    itemsToRemove.forEach(function(x) {
        helpers.debug('Removing key: ' + x + 'from the cache');
        if (x) {
            kony.store.removeItem(x);
        }
    });
};
//Instantiation
var DAL = new services.DAL();
/********************************************************************
                          END DATA ACCESS LAYER
  ********************************************************************/