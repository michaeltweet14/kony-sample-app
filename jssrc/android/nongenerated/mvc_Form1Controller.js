// jshint esnext: true
/********************************************************************
                         Form1 CONTROLLER CLASS
********************************************************************/
//Constructor
mvc.Form1Controller = function(views) {
    mvc.FormController.call(this, views, true, false);
    this.form.onDeviceBack = function() {}; //Do nothing on back so they dont navigate to the splash page.
    this.getData();
};
//Set the Controller to inherit from the FormController base class
mvc.Form1Controller.prototype = Object.create(mvc.FormController.prototype);
//Set the Controller constructor to use that which is defined on the Form1Controller
mvc.Form1Controller.prototype.constructor = mvc.Form1Controller;
/****************
    METHODS
****************/
mvc.Form1Controller.prototype.getData = function() {
    var context = this;
    var showSpinner = true;
    var dataFunctions = [CONSTANTS.DAOs.Offices];
    DAL.getValidData(dataFunctions, showSpinner).then(function(values) {
        context.populateForm(values[0]);
    }).fail(function(error) {
        context.populateErrors();
        helpers.error(error);
    });
};
mvc.Form1Controller.prototype.populateForm = function(offices) {
    var context = this;
    var widget;
    var mapImage = function(office) {
        return CONSTANTS.imageMap[office];
    };
    helpers.debug('in populate form :: ' + JSON.stringify(offices));
    offices.forEach(function(office, index) {
        helpers.debug('onclick');
        var onClick = function() {
            context.navigateTo(eval(helpers.officeNavigationHelper(office.Office)));
        };
        helpers.debug('Creating widget for : ' + office.Office);
        widget = new widgets.OfficeButton(office.Office, mapImage(office.Office), onClick);
        if (index % 2) {
            context.officeContainer.LeftColumn.addAt(widget, 0);
        } else {
            context.officeContainer.RightColumn.addAt(widget, 0);
        }
    });
};
mvc.Form1Controller.prototype.populateErrors = function() {
    var context = this;
    helpers.debug('in populateErrors');
};
/********************************************************************
                        END HOME CONTROLLER CLASS
********************************************************************/