//startup.js file
var globalhttpheaders = {};
var appConfig = {
    appId: "SampleApp",
    appName: "SampleApp",
    appVersion: "1.0.0",
    platformVersion: null,
    serverIp: "172.20.205.11",
    serverPort: "80",
    secureServerPort: "443",
    isDebug: true,
    middlewareContext: "SampleApp",
    isMFApp: false,
    eventTypes: ["FormEntry", "ServiceRequest", "Error", "Crash"],
    url: "https://engie-dev.konycloud.com/SampleApp/MWServlet",
    secureurl: "https://engie-dev.konycloud.com/SampleApp/MWServlet"
};
sessionID = "";

function appInit(params) {
    skinsInit();
    initializeMVCTemplates();
    initializeUserWidgets();
    Form1Globals();
    OfficeGlobals();
    TemplateErrorGlobals();
    TemplateHeaderGlobals();
    TemplateMenuGlobals();
    TemplateOfficeButtonGlobals();
    TemplateOfficeFormGlobals();
    setAppBehaviors();
};

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        APILevel: 8000
    })
};

function themeCallBack() {
    initializeGlobalVariables();
    callAppMenu();
    kony.application.setApplicationInitializationEvents({
        preappinit: AS_AppEvents_b0c3449d7afc405d9ab3037f2df06e1a,
        init: appInit,
        showstartupform: function() {
            Form1.show();
        }
    });
};

function loadResources() {
    globalhttpheaders = {};
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_KonyLogger"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_binarydata"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.ND_binary_util"
    });
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "eventTypes": appConfig.eventTypes,
    }
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
};

function onSuccessSDKCallBack() {
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
//If default locale is specified. This is set even before any other app life cycle event is called.
loadResources();
// If you wish to debug Application Initialization events, now is the time to
// place breakpoints.
debugger;