function addWidgetsTemplateMenu() {
    TemplateMenu.setDefaultUnit(kony.flex.DP);
    var Menu = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "Menu",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "MenuBg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    Menu.setDefaultUnit(kony.flex.DP);
    var ButtonContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "ButtonContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    ButtonContainer.setDefaultUnit(kony.flex.DP);
    ButtonContainer.add();
    Menu.add(ButtonContainer);
    var MenuButton = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "CopyslFbox0e922993012924d",
        "height": "10%",
        "id": "MenuButton",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    MenuButton.setDefaultUnit(kony.flex.DP);
    var ButtonText = new kony.ui.Label({
        "height": "100%",
        "id": "ButtonText",
        "isVisible": true,
        "left": "7.50%",
        "skin": "MenuButtonTextBase",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    MenuButton.add(ButtonText);
    TemplateMenu.add(Menu, MenuButton);
};

function TemplateMenuGlobals() {
    TemplateMenu = new kony.ui.Form2({
        "addWidgets": addWidgetsTemplateMenu,
        "enabledForIdleTimeout": false,
        "id": "TemplateMenu",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "slForm"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};