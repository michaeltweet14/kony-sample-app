function addWidgetsTemplateHeader() {
    TemplateHeader.setDefaultUnit(kony.flex.DP);
    var MenuHdr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "7%",
        "id": "MenuHdr",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "MenuHdrContainer",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    MenuHdr.setDefaultUnit(kony.flex.DP);
    var MenuHdrNavButton = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "MenuHdrNavButton",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {}, {});
    MenuHdrNavButton.setDefaultUnit(kony.flex.DP);
    var MenuHdrNavImage = new kony.ui.Image2({
        "centerY": "50%",
        "height": "80%",
        "id": "MenuHdrNavImage",
        "isVisible": true,
        "left": "10%",
        "skin": "slImage",
        "src": "hamburger.png",
        "width": "35%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    MenuHdrNavButton.add(MenuHdrNavImage);
    var MenuHdrFormTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "MenuHdrFormTitle",
        "isVisible": true,
        "left": "0dp",
        "skin": "MenuHeaderText",
        "text": "Label1",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    MenuHdr.add(MenuHdrNavButton, MenuHdrFormTitle);
    var BackHdr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "7%",
        "id": "BackHdr",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "MenuHdrContainer",
        "top": "10dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    BackHdr.setDefaultUnit(kony.flex.DP);
    var BackHdrNavButton = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "BackHdrNavButton",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "70dp",
        "zIndex": 1
    }, {}, {});
    BackHdrNavButton.setDefaultUnit(kony.flex.DP);
    var BackHdrNavImage = new kony.ui.Image2({
        "centerY": "50%",
        "height": "80%",
        "id": "BackHdrNavImage",
        "isVisible": true,
        "left": "10%",
        "skin": "slImage",
        "src": "left_arrow.png",
        "width": "35%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    BackHdrNavButton.add(BackHdrNavImage);
    var BackHdrFormTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "BackHdrFormTitle",
        "isVisible": true,
        "left": "0dp",
        "skin": "MenuHeaderText",
        "text": "Label1",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    BackHdr.add(BackHdrNavButton, BackHdrFormTitle);
    var Group = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "Group",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    Group.setDefaultUnit(kony.flex.DP);
    var CopyMenuHdrFormTitle0j9ea7b7e708a46 = new kony.ui.Label({
        "centerY": "50%",
        "id": "CopyMenuHdrFormTitle0j9ea7b7e708a46",
        "isVisible": true,
        "right": "50%",
        "skin": "MenuHeaderText",
        "text": "Label1",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyMenuHdrFormTitle0e78730a1b17d46 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "CopyMenuHdrFormTitle0e78730a1b17d46",
        "isVisible": true,
        "skin": "CopyslLabel0f610dd711ed74a",
        "text": " Label1 Label2",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    Group.add(CopyMenuHdrFormTitle0j9ea7b7e708a46, CopyMenuHdrFormTitle0e78730a1b17d46);
    TemplateHeader.add(MenuHdr, BackHdr, Group);
};

function TemplateHeaderGlobals() {
    TemplateHeader = new kony.ui.Form2({
        "addWidgets": addWidgetsTemplateHeader,
        "enabledForIdleTimeout": false,
        "id": "TemplateHeader",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": true,
        "skin": "FormBg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};