function addWidgetsTemplateError() {
    TemplateError.setDefaultUnit(kony.flex.DP);
    var ErrorMessage = new kony.ui.Label({
        "height": "100%",
        "id": "ErrorMessage",
        "isVisible": true,
        "left": "0dp",
        "skin": "ErrorMessage",
        "text": "Unable to retrieve data. Please try again later.",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    TemplateError.add(ErrorMessage);
};

function TemplateErrorGlobals() {
    TemplateError = new kony.ui.Form2({
        "addWidgets": addWidgetsTemplateError,
        "enabledForIdleTimeout": false,
        "id": "TemplateError",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "FormBg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};