function addWidgetsTemplateOfficeButton() {
    TemplateOfficeButton.setDefaultUnit(kony.flex.DP);
    var OfficeContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "19%",
        "id": "OfficeContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "CopyslFbox0h5a7b67bfeb343",
        "top": "1%",
        "width": "98%",
        "zIndex": 1
    }, {}, {});
    OfficeContainer.setDefaultUnit(kony.flex.DP);
    var Skyline = new kony.ui.Image2({
        "centerX": "50%",
        "height": "100%",
        "id": "Skyline",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "slider.png",
        "top": "0dp",
        "width": "200%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var LabelContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": false,
        "height": "25%",
        "id": "LabelContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    LabelContainer.setDefaultUnit(kony.flex.DP);
    var Office = new kony.ui.Label({
        "centerX": "50%",
        "height": "100%",
        "id": "Office",
        "isVisible": true,
        "skin": "CopyslLabel0bf4aecca0fb442",
        "text": "City",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    LabelContainer.add(Office);
    var OverlayContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "OfficeOnClick",
        "height": "100%",
        "id": "OverlayContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    OverlayContainer.setDefaultUnit(kony.flex.DP);
    OverlayContainer.add();
    OfficeContainer.add(Skyline, LabelContainer, OverlayContainer);
    TemplateOfficeButton.add(OfficeContainer);
};

function TemplateOfficeButtonGlobals() {
    TemplateOfficeButton = new kony.ui.Form2({
        "addWidgets": addWidgetsTemplateOfficeButton,
        "enabledForIdleTimeout": false,
        "id": "TemplateOfficeButton",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "slForm"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};