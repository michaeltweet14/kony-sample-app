function addWidgetsForm1() {
    Form1.setDefaultUnit(kony.flex.DP);
    var ContentArea = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "ContentArea",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    ContentArea.setDefaultUnit(kony.flex.DP);
    var MainContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "MainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "DefaultBg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    MainContent.setDefaultUnit(kony.flex.DP);
    var OfficeContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "OfficeContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    OfficeContainer.setDefaultUnit(kony.flex.DP);
    var LeftColumn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "LeftColumn",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    LeftColumn.setDefaultUnit(kony.flex.DP);
    LeftColumn.add();
    var Div = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "98%",
        "id": "Div",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "2%",
        "zIndex": 1
    }, {}, {});
    Div.setDefaultUnit(kony.flex.DP);
    var InnerDiv = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "InnerDiv",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "Divider",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    InnerDiv.setDefaultUnit(kony.flex.DP);
    InnerDiv.add();
    Div.add(InnerDiv);
    var RightColumn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "RightColumn",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {}, {});
    RightColumn.setDefaultUnit(kony.flex.DP);
    RightColumn.add();
    OfficeContainer.add(LeftColumn, Div, RightColumn);
    var LogoContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "LogoContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    LogoContainer.setDefaultUnit(kony.flex.DP);
    var Logo = new kony.ui.Image2({
        "centerY": "50%",
        "height": "80%",
        "id": "Logo",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "pariveda_color_horiz.png",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    LogoContainer.add(Logo);
    MainContent.add(OfficeContainer, LogoContainer);
    ContentArea.add(MainContent);
    Form1.add(ContentArea);
};

function Form1Globals() {
    Form1 = new kony.ui.Form2({
        "addWidgets": addWidgetsForm1,
        "enabledForIdleTimeout": false,
        "id": "Form1",
        "init": AS_Form_cfaacab787024ceea8b686831841a69c,
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "FormBg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};