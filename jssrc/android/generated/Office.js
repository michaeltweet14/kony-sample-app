function addWidgetsOffice() {
    Office.setDefaultUnit(kony.flex.DP);
    var ContentArea = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "ContentArea",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    ContentArea.setDefaultUnit(kony.flex.DP);
    var MainContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "MainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "DefaultBg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    MainContent.setDefaultUnit(kony.flex.DP);
    MainContent.add();
    ContentArea.add(MainContent);
    Office.add(ContentArea);
};

function OfficeGlobals() {
    Office = new kony.ui.Form2({
        "addWidgets": addWidgetsOffice,
        "enabledForIdleTimeout": false,
        "id": "Office",
        "init": AS_Form_bcef9900bf1547a98dde2bef9b8b576a,
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "FormBg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};