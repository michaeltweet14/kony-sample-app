function addWidgetsTemplateOfficeForm() {
    TemplateOfficeForm.setDefaultUnit(kony.flex.DP);
    var Office = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "Office",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "skin": "DefaultBg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    Office.setDefaultUnit(kony.flex.DP);
    var DescriptionColumn = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "90%",
        "horizontalScrollIndicator": true,
        "id": "DescriptionColumn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "10%",
        "verticalScrollIndicator": true,
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    DescriptionColumn.setDefaultUnit(kony.flex.DP);
    var Description = new kony.ui.Label({
        "height": "100%",
        "id": "Description",
        "isVisible": true,
        "left": "0dp",
        "skin": "DescriptionText",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    DescriptionColumn.add(Description);
    var InfoColumn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "InfoColumn",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "3%",
        "skin": "slFbox",
        "top": "10%",
        "width": "35%",
        "zIndex": 1
    }, {}, {});
    InfoColumn.setDefaultUnit(kony.flex.DP);
    var AddressContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "AddressContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    AddressContainer.setDefaultUnit(kony.flex.DP);
    var AddressHeader = new kony.ui.Label({
        "id": "AddressHeader",
        "isVisible": true,
        "left": "0dp",
        "skin": "InfoHeader",
        "text": "Address",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var AddressText = new kony.ui.Label({
        "id": "AddressText",
        "isVisible": true,
        "left": "0",
        "skin": "InfoText",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    AddressContainer.add(AddressHeader, AddressText);
    var PhoneContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "PhoneContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "4%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    PhoneContainer.setDefaultUnit(kony.flex.DP);
    var PhoneHeader = new kony.ui.Label({
        "id": "PhoneHeader",
        "isVisible": true,
        "left": "0dp",
        "skin": "InfoHeader",
        "text": "Phone",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var PhoneText = new kony.ui.Label({
        "id": "PhoneText",
        "isVisible": true,
        "left": "0",
        "skin": "InfoText",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    PhoneContainer.add(PhoneHeader, PhoneText);
    var FaxContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FaxContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "4%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FaxContainer.setDefaultUnit(kony.flex.DP);
    var FaxHeader = new kony.ui.Label({
        "id": "FaxHeader",
        "isVisible": true,
        "left": "0dp",
        "skin": "InfoHeader",
        "text": "Fax",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var FaxText = new kony.ui.Label({
        "id": "FaxText",
        "isVisible": true,
        "left": "0",
        "skin": "InfoText",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FaxContainer.add(FaxHeader, FaxText);
    var EmailContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "EmailContainer",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "top": "4%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    EmailContainer.setDefaultUnit(kony.flex.DP);
    var EmailHeader = new kony.ui.Label({
        "id": "EmailHeader",
        "isVisible": true,
        "left": "0dp",
        "skin": "InfoHeader",
        "text": "Email",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var EmailText = new kony.ui.Label({
        "id": "EmailText",
        "isVisible": true,
        "left": "0",
        "skin": "InfoText",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    EmailContainer.add(EmailHeader, EmailText);
    InfoColumn.add(AddressContainer, PhoneContainer, FaxContainer, EmailContainer);
    Office.add(DescriptionColumn, InfoColumn);
    TemplateOfficeForm.add(Office);
};

function TemplateOfficeFormGlobals() {
    TemplateOfficeForm = new kony.ui.Form2({
        "addWidgets": addWidgetsTemplateOfficeForm,
        "enabledForIdleTimeout": false,
        "id": "TemplateOfficeForm",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": true,
        "skin": "FormBg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};