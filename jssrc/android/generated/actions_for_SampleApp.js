//actions.js file 
function AS_Atlanta_Init() {
    return AS_NamedActions_ec1525275f4d4231a4485814d06475e4();
}

function AS_NamedActions_ec1525275f4d4231a4485814d06475e4() {}

function AS_Form1_init(eventobject) {
    return AS_Form_cfaacab787024ceea8b686831841a69c(eventobject);
}

function AS_Form_cfaacab787024ceea8b686831841a69c(eventobject) {
    helpers.log('init Called for ----' + this.id + '----');
    var views = {
        form: this,
        officeContainer: this.OfficeContainer
    };
    var controller = new mvc.Form1Controller(views);
    mvc.register(this.id, this, controller);
}

function AS_Houston_Init(eventobject) {
    return AS_Form_bcef9900bf1547a98dde2bef9b8b576a(eventobject);
}

function AS_Form_bcef9900bf1547a98dde2bef9b8b576a(eventobject) {
    helpers.log('init Called for ----' + this.id + '----');
    var views = {
        form: this,
        mainContent: this.MainContent
    };
    var controller = new mvc.OfficeController(views);
    mvc.register(this.id, this, controller);
}

function AS_AppEvents_b0c3449d7afc405d9ab3037f2df06e1a(eventobject) {
    kony.application.setApplicationBehaviors({
        "hideDefaultLoadingIndicator": true
    });
}